# FH Tech Interview Test (Set C)
For more info, please take so time to the [Presentation Video](https://youtu.be/Ti3QbVYsiEg )
![image1](https://i.imgur.com/sMOjlvY.png)

- [Link of resource](#Link_of_resource)
- [Analytical Thinking - Pages & sessions](#Analytical_Thinking-Pages_&_sessions)
    - [Identify the problem](#Identify_the_problem)
    - [Possible reasons](#Possible_reasons)
- [System design - Drive clients to reply to more creatives](#System_design-Drive_clients_to_reply_to_more_creatives)
    - [Minimize the chances of email falling into spam folder](#Minimize_the_chances_of_email_falling_into_spam_folder)
    - [Propose a system/feature allow clients to reply lazier and more convenient](#Propose_a_system/feature_allow_clients_to_reply_lazier_and_more_convenient)
    - [Suggest any other ways to allow more clients to reply to more creatives.](#Suggest_any_other_ways_to_allow_more_clients_to_reply_to_more_creatives.)

## Link of resource
[Presentation Video](https://youtu.be/Ti3QbVYsiEg )<br >
[PPT Link](https://www.canva.com/design/DADyGMFo9iY/yXXEnn1_A9F7Z_YUl3-zmA/view?utm_content=DADyGMFo9iY&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton#1)

## Analytical Thinking - Pages & sessions
### Identify the problem
- Total Pages Visited >> Total Sessionsw		
- Data flow ratio (1:9 -1:3)	
- both the number of premium account being activated and the number of posted jobs remains the same	
- Non-DDOS / Hacking	
### Possible reasons
- Tech Level Reason 
  1. Default Session Time (30min)?
  2. Users Keep refresh pages, bugs?
  3. Google DOC
- User Level Reason
  1. Why users need to refresh the pages?
  2. Why they stay in the same session?
  3. Highly viewed pages, high bounce rates Low dwell time
- Other Reason

## System design - Drive clients to reply to more creatives
### Minimize the chances of email falling into spam folder
- Email reputation (Mail server, Volume of emails, Relevance, Value of list etc.)
- Relationship Between Subscriber 
  1. Send a valid email to subscriber when they register an new account
- Content of Email
  1. Customization
  2. Hyperlink
  3. Image
### Propose a system/feature allow clients to reply lazier and more convenient
- Annoying for the clients to go back to the site and login
- Most of the clients check email from their phones
- UX Problem
- Through email
![image2](https://i.imgur.com/bOScLYg.png)
### Suggest any other ways to allow more clients to reply to more creatives.
- Bot Channel
  1. Easy using
  2. Quite replyMuti channel (Whatsapp, Line, wechat, kakaotalk etc.)  
  3. Drive your client and keep them stay in your apps  
  4. HK - World / World - World (Expand clients region)
  5. e.g [LineBot](https://github.com/WilsonLTL/LINEBot-kit)
![image3](https://i.imgur.com/NF1AL4Y.png)
![image4](https://i.imgur.com/OD1nWFC.png)